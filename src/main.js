import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入字体的文件
import './assets/font/iconfont.css'

import SocketService from './utils/socket_service'
// 引入全局的样式文件
import './assets/css/global.less'

import axios from 'axios'

// 请求基准路径的配置
// koa2中app,js
// 3.绑定端口号 8888
// app.listen(1749)
axios.defaults.baseURL = 'http://127.0.0.1:1749/api/'
// 将axios挂载到Vue的原型对象上
// 在别的组件中 this.$http
Vue.prototype.$http = axios

// 将全局的echarts对象挂载到Vue的原型对象上
// 别的组件中 this.$echarts
Vue.prototype.$echarts = window.echarts

Vue.config.productionTip = false

// 调用websocket 对服务端连接
SocketService.Instance.connect()
// 其他的组件 this.$socket
Vue.prototype.$socket = SocketService.instance

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
